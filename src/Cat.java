public class Cat {
    private String Name;
    private int Age;
    private String Breed;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }

    public String getBreed() {
        return Breed;
    }

    public void setBreed(String breed) {
        Breed = breed;
    }

    public Cat(String name, int age, String breed) {
        Name = name;
        Age = age;
        Breed = breed;
    }


    public void Voice() {
        System.out.println("Meow!!!");
    }

    public void GrowUp(int age) {
        setAge(age);
        System.out.println(Name + "'s age now is " + this.Age);
    }
}
