public class Main {
    public static void main(String[] args) {
        Cat Micky  = new Cat("Micky", 4, "British");
        Micky.Voice();
        Micky.GrowUp(5);

        Cat Jack  = new Cat("Jack", 2, "Siam");
        int currentAge = Jack.getAge();
        System.out.println("Current age is "+currentAge);
        Jack.Voice();
        Jack.GrowUp(3);
    }
}
